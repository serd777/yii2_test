<?php namespace app\controllers;

use app\models\forms\AuthForm;
use Yii;
use yii\web\Controller;

/**
 * Контроллер авторизации
 * @package app\controllers
 */
class AuthController extends Controller {
	/**
	 * Основная страница
	 * @return string
	 */
	public function indexAction() {
		return $this->renderView(new AuthForm());
	}

	/**
	 * Обработка входа
	 * @return \yii\web\Response|string
	 */
	public function signInAction() {
		//  Данный роут только для гостей
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		//  Обрабатываем входные данные
		$authForm = new AuthForm();
		if ($authForm->load(Yii::$app->request->post())) {
			//  Если данные переданы верно
			if ($authForm->signIn()) {
				//  Валидация и авторизация успешны
				return $this->redirect('/admin');
			}
		}

		//  По канонам обнулим пароль
		$authForm->password = '';

		//  Что-то пошло не так :) рисуем страницу авторизации
		return $this->renderView($authForm);
	}

	/**
	 * Обработка выхода
	 * @return \yii\web\Response
	 */
	public function signOutAction() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	/**
	 * Возвращает готовый html страницы авторизации
	 * @param AuthForm $model
	 *
	 * @return string
	 */
	private function renderView(AuthForm $model) {
		return $this->render('index', [
			'model' => $model
		]);
	}
}