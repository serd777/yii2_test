<?php namespace app\controllers;

use app\models\Ticket;
use yii\web\Controller;

/**
 * Контроллер админ панели
 * @package app\controllers
 */
class AdminController extends Controller {
	/**
	 * Отображает страницу с тикетами
	 */
	public function actionIndex() {
		//  ToDo: добавить пагинацию
		$tickets = Ticket::find()->all();
		return $this->render('index', [
			'tickets' => $tickets
		]);
	}

	/**
	 * Закрывает указанный тикет
	 *
	 * @param int $id
	 *
	 * @return \yii\web\Response
	 */
	public function actionTicketClose($id) {
		if (!is_numeric($id)) {
			return $this->goBack();
		}

		$ticket = Ticket::findOne($id);
		if (!$ticket) {
			return $this->goBack();
		}

		$ticket->setStatus(Ticket::STATUS_CLOSED);
		$ticket->save();

		return $this->goBack();
	}
}