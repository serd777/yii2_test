<?php namespace app\models\forms;

use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Модель формы авторизации
 * @package app\models\forms
 */
class AuthForm extends Model {
	/**
	 * Имя пользователя
	 * @var string
	 */
	public $username;
	/**
	 * Пароль
	 * @var string
	 */
	public $password;
	/**
	 * Пользователь
	 * @var User|null
	 */
	private $user = null;

	/**
	 * Попытка авторизовать пользователя
	 * @return bool
	 */
	public function signIn() {
		if ($this->validate()) {
			return Yii::$app->user->login($this->getUser(), 3600 * 24 * 30);
		}
		return false;
	}

	/**
	 * Фильтрация данных
	 * @return array
	 */
	public function rules() {
		return [
			[['username', 'password'], 'required'],
			['password', 'validatePassword'],
		];
	}

	/**
	 * Валидация пароля
	 * @param $attribute
	 * @param $params
	 */
	public function validatePassword($attribute, $params) {
		if (!$this->hasErrors()) { // Если есть смысл проверять
			$user = $this->getUser();

			if (!$user || !$user->validatePassword($this->password, $params)) {
				//  Валидация провалилась
				$this->addError($attribute, 'Incorrect username or password.');
			}
		}
	}

	/**
	 * Возвращает текущего юзера
	 * @return mixed|null|static
	 */
	private function getUser() {
		if (!$this->user) { // Если не инициализировали пользователя
			$this->user = User::findByUsername($this->username);
		}
		return $this->user;
	}
}