<?php namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * Модель тикета
 * @package app\models
 */
class Ticket extends ActiveRecord {
	//
	//  Статусы тикетов
	//
	const STATUS_NEW      = 'new';
	const STATUS_ANSWERED = 'answered';
	const STATUS_CLOSED   = 'closed';
	const STATUS_OPENED   = 'opened';

	/**
	 * Список доступных статусов
	 * @var array
	 */
	static $availableStatues = [
		self::STATUS_NEW,
		self::STATUS_ANSWERED,
		self::STATUS_CLOSED,
		self::STATUS_OPENED
	];

	/**
	 * Тема тикета
	 * @var string
	 */
	public $title;

	/**
	 * Поля модели
	 * @return array
	 */
	public function fields() {
		return [
			'id',
			'user_id',
			'title',
			'status'
		];
	}

	/**
	 * Возвращает ID записи
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Возвращает связь с пользователем
	 * @return ActiveQuery
	 */
	public function getUser(): ActiveQuery {
		return $this->hasOne(User::class, ['id' => 'user_id']);
	}

	/**
	 * Возвращаем статус
	 * @return string
	 */
	public function getStatus(): string {
		return $this->status;
	}

	/**
	 * Задает статус тикета
	 * @param string $status
	 */
	public function setStatus(string $status) {
		if (!in_array($status, static::$availableStatues)) {
			new Exception("Wrong ticket status");
		}
		$this->status = $status;
	}
}