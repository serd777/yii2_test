<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $tickets array */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'TicketsList';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
	<h1>Список тикетов</h1>

	<table>
		<thead>
			<tr>
				<th>title</th>
				<th>username</th>
				<th>status</th>
				<th>actions</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($tickets as $ticket) { ?>
				<tr>
					<td><?php Html::encode($ticket->title) ?></td>
					<td><?php Html::encode($ticket->user->username) ?></td>
					<td><?php Html::encode($ticket->status) ?></td>
					<td>
						<a href="<?php Html::encode(Url::toRoute(['/admin/ticket/close', 'id' => $ticket->id])) ?>">Удалить</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
